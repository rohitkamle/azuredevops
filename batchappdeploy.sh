
#!/bin/bash


echo "Hello World"
echo "AGENT_WORKFOLDER is $AGENT_WORKFOLDER"
echo "AGENT_WORKFOLDER contents:"
ls -l $AGENT_WORKFOLDER
echo "AGENT_BUILDDIRECTORY is $AGENT_BUILDDIRECTORY"
echo "AGENT_BUILDDIRECTORY contents:"
ls -l $AGENT_BUILDDIRECTORY
echo "SYSTEM_HOSTTYPE is $SYSTEM_HOSTTYPE"
echo "Over and out."




#get Azure version 
az --version

#list batch accounts
az batch account list


#Create Resource Group
az group create -l westus -n BatchJobsRG

#Create Storage Account
az storage account create --name 'storageaccforbatchjob' --resource-group 'BatchJobsRG'

#Create Batch Account
az batch account create --location 'West US' --name 'newjavabatchjobaccwus' --resource-group 'BatchJobsRG' --storage-account 'storageaccforbatchjob'

#Enable auto-storage for Batch account
az batch account autostorage-keys sync --name 'newjavabatchjobaccwus' --resource-group 'BatchJobsRG'

#Create batch Application
az batch application create --application-name 'javaapp1' --name 'newjavabatchjobaccwus'  --resource-group 'BatchJobsRG'

#create application package
az batch application package create \
  --resource-group BatchJobsRG \
  --name newjavabatchjobaccwus \
  --application-name javaapp1 \
  --package-file App1.zip \
  --version 1.0
  
  
#create Pool
az batch pool create \
 --account-name newjavabatchjobaccwus \
 --account-endpoint newjavabatchjobaccwus.westus.batch.azure.com \
--id javapool02 \
 --target-dedicated 1 \
 --image MicrosoftWindowsServer:WindowsServer:2016-Datacenter:latest \
 --node-agent-sku-id "batch.node.windows amd64" \
 --vm-size Standard_A1 \
 --application-package-references javaapp1#1.0 \
 
#get Azure version 
az --version
 
 

